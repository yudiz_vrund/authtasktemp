import React, { useState, useEffect } from "react";
import { Modal, Table, ModalBody, Button } from "reactstrap";
import axios from "axios";
import "./index.css";
///import Pagin from './Pagin';
//import NewPagin from "./NewPagin";
import { useHistory } from "react-router-dom";
//import queryString from "query-string";
//import { number } from "prop-types";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
//import { useHistory } from 'react-router-dom';
import { useLocation } from "react-router-dom";

const Dashboard = () => {
  const [sports, setSports] = useState([]);

  // eslint-disable-next-line no-unused-vars
  const [currentPage, setCurrentPage] = useState(1);

  // eslint-disable-next-line no-unused-vars
  const [sportsPerPage, setSportsPerPage] = useState(1);

  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  const history = useHistory();

  const [title, setTitle] = useState();
  const [desc, setDesc] = useState();
  const [pic, setPic] = useState();

  const search = useLocation().search;
  console.log(search);
  const q = new URLSearchParams(search).get("q");
  console.log(q);
  //const history = useHistory();
  useEffect(() => {
    //const param = queryString.parse(location.search);
    // console.log(location.search);
    // const search = location.search;
    // const params = new URLSearchParams(search);
    // const param = params.get("q");
    // console.log("page number is :", Number(param));

    //just try to this code
    if(Number(q) === 1){
      const getSports1 = async () => {
        const limit = { nStart: 0, nLimit: 2 };
        const res = await axios.post(
          "https://backend.sports.info/api/v1/posts/recent",
          limit
        );
        setSports(res.data.data);
      };
  
      getSports1();
    }
    

    if (Number(q) === 2) {
      const getSports2 = async () => {
        const limit = { nStart: 2, nLimit: 2 };
        const res = await axios.post(
          "https://backend.sports.info/api/v1/posts/recent",
          limit
        );
        //console.log(res.data.data);
        setSports(res.data.data);
      };

      getSports2();
    }

    else{
      const getSports1 = async () => {
        const limit = { nStart: 0, nLimit: 2 };
        const res = await axios.post(
          "https://backend.sports.info/api/v1/posts/recent",
          limit
        );
        setSports(res.data.data);
      };
  
      getSports1();
    }
  }, [q]);

  //   useEffect(() => {
  //     const secondData = async () => {
  //       const limit = { nStart: 2, nLimit: 4 };
  //       const res = await axios.post(
  //         "https://backend.sports.info/api/v1/posts/recent",
  //         limit
  //       );
  //       console.log(res.data.data);
  //       setSports(res.data.data);
  //     };

  //     secondData();
  //   }, []);

  // useEffect(()=>{
  //     const params = new URLSearchParams(location.search);
  //     console.log(params);

  // },[])

  // const indexOfLastSport = currentPage * sportsPerPage;
  // const indexOfFirstSport = indexOfLastSport - sportsPerPage;
  // const currentSports = sports.slice(indexOfFirstSport, indexOfLastSport);

  //const paginate = (pageNumber) => setCurrentPage(pageNumber);

  // const next_paginate  = (pageNumber) => setCurrentPage(pageNumber+1);

  const openModel = (title, desc, img) => {
    setModal(true);
    setTitle(title);
    setDesc(desc);
    setPic(img);
  };

  const handleLogout = () => {
    localStorage.clear();
    history.replace("/");
  };

  return (
    <>
      <Table bordered>
        <thead>
          <tr>
            <th>Title</th>
            <th>View Count</th>
            <th>Profile</th>
          </tr>
        </thead>
        <tbody>
          {sports.map((sport) => (
            <tr
              key={sport._id}
              onClick={() =>
                openModel(sport.sTitle, sport.sDescription, sport.sImage)
              }
            >
              <td>{sport.sTitle}</td>
              <td>{sport.nViewCounts}</td>
              <td>
                <img src={sport.sImage} height={100} width={100}></img>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      {/* <Pagin sportsPerPage={sportsPerPage} totalSports={sports.length} paginate={paginate}  /> */}
      {/* <NewPagin /> */}
      <Pagination>
        <PaginationItem>
          <PaginationLink href="?q=1">1</PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink href="?q=2">2</PaginationLink>
        </PaginationItem>
      </Pagination>

      <Modal isOpen={modal} toggle={toggle}>
        <ModalBody>
          <h5>{title}</h5>
          <br />
          <img src={pic} width="400" height="300"></img>
          <br />
          {desc}
        </ModalBody>
      </Modal>

      <Button size="lg" color="danger" outline onClick={handleLogout}>
        {" "}
        Logout
      </Button>
      {/* {sports.map(sport => (
                <li key={sport._id}>
                    {sport.sTitle}
                </li>
            ))} */}
    </>
  );
};

export default Dashboard;
