import React from "react";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import { useHistory } from 'react-router-dom';

const NewPagin = () => {

    const history = useHistory();

  return (
    <>
      <Pagination>
        <PaginationItem>
          <PaginationLink href="?q=1">1</PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink onClick={history.push(`/dashboard/?q=2`)} >2</PaginationLink>
        </PaginationItem>
      </Pagination>
    </>
  );
};

export default NewPagin;
